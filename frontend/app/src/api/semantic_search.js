import { BACKEND_URL } from '../utils/globals'

/**
 * Get the semantically closest elements from the dataset to query
 * 
 * @param {string} text 
 * @param {function({text: string, score: float}[]):void} success_callback 
 * @param {function():void} blank_callback 
 * @param {function():void} fail_callback 
 */
export function get_closest_elements(text, success_callback, blank_callback, fail_callback) {
    const args = {
        'text': text
    }

    fetch(BACKEND_URL + '/semantic_search', {
        method: 'POST',
        body: JSON.stringify(args),
        headers: { "Content-type": "application/json;charset=UTF-8" }
    })
        .then(response => response.json())
        .then(data => {
            if (Object.keys(data).length === 0) {
                blank_callback();
            } else {
                success_callback(data);
            }
        }).catch((e) => {
            fail_callback();
        })
}
