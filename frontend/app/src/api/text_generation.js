/**
 * Get the generated text for the specified prompt
 * 
 * @param {string} text 
 * @param {function({text: string, score: float}[]):void} success_callback 
 * @param {function():void} blank_callback 
 * @param {function():void} fail_callback 
 */


export function get_generated_text(text, apiKey, success_callback, blank_callback, fail_callback) {
    const API_URL = "https://api-inference.huggingface.co/models/EleutherAI/gpt-neo-2.7B"

    fetch(API_URL, {
        method: 'POST',
        body: text,
        headers: { "Authorization": "Bearer " + apiKey }
        })
        .then(response => response.json())
        .then(data => {
            if (data.length === 0) {
                blank_callback();
            } else {
                success_callback(data[0]['generated_text']);
            }
        }).catch((e) => {
            fail_callback();
        })
}
