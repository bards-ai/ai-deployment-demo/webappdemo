import { IMAGENET_LABELS } from '../utils/globals'
import ndarray from 'ndarray'
import ops from 'ndarray-ops';
const ort = require('onnxruntime-web');

export async function inference(session, image) {
    try {
        var img = new Image();
        img.src = image
        const width = 224;
        const height = 224;

        var canvas = document.createElement("canvas");
        canvas.width = 224;
        canvas.height = 224;
        var ctx = canvas.getContext("2d");
        ctx.width = width;
        ctx.height = height;
        ctx.drawImage(img, 0, 0, width, height);
        var imgData = ctx.getImageData(0, 0, width, height).data;
        const dataTensor = ndarray(new Float32Array(imgData), [width, height, 4]);
        const dataProcessedArray = ndarray(new Float32Array(width * height * 3), [1, 3, width, height]);
        ops.assign(dataProcessedArray.pick(0, 0, null, null), dataTensor.pick(null, null, 2));
        ops.assign(dataProcessedArray.pick(0, 1, null, null), dataTensor.pick(null, null, 1));
        ops.assign(dataProcessedArray.pick(0, 2, null, null), dataTensor.pick(null, null, 0));
        ops.divseq(dataProcessedArray, 255);
        ops.subseq(dataProcessedArray.pick(0, 0, null, null), 0.485);
        ops.subseq(dataProcessedArray.pick(0, 1, null, null), 0.456);
        ops.subseq(dataProcessedArray.pick(0, 2, null, null), 0.406);
        ops.divseq(dataProcessedArray.pick(0, 0, null, null), 0.229);
        ops.divseq(dataProcessedArray.pick(0, 1, null, null), 0.224);
        ops.divseq(dataProcessedArray.pick(0, 2, null, null), 0.225);
        const dims = [1, 3, 224, 224]
        const feeds = { input1: new ort.Tensor("float32", dataProcessedArray.data, dims) }
        
        const results = await session.run(feeds)
        var output  = results.output1.data
        var valueMax = Math.max.apply(null, output)
        var indexMax = output.indexOf(valueMax)
        return IMAGENET_LABELS[indexMax]
    } catch (e) {
        console.log(e)
    }
}
