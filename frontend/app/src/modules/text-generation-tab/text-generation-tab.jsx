import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Link from "@material-ui/core/Link";

import { get_generated_text } from "../../api/text_generation";
import MenuButton from "../menu-button/menu-button";
import { useLocalStorage } from "../../utils/localStorage"

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "60rem",
    margin: "auto"
  },
  sendButton: {
    marginTop: 15,
    marginBottom: 50,
    alignItems: "center",
  },
  examplesButton: {
    marginTop: 15,
    marginBottom: 50,
  },
  grid: {
    margin: 0,
  },
  tab: {
    marginLeft: 10,
    marginRight: 10,
  },
  paper: {
    padding: 20,
  },
  resultBox: {
    whiteSpace: "pre-line",
    textAlign: "left"
  },
  section: {
    textAlign: 'left',
  }
}));

export default function TextGenerationTab() {
  const classes = useStyles();
  let [inputText, setInputText] = useState("");
  let [text, setText] = useState("");
  let [generatedText, setGeneratedText] = useState("");
  let [loading, setLoading] = useState(false);
  let [apiKey, setApiKey] = useLocalStorage("apiKey", "");

  const show_elements = () => {
    if (apiKey === "") {
      setText("Enter API key!");
      return;
    }
    setLoading(true);
    get_generated_text(
      inputText,
      apiKey,
      (generated_text) => {
        setText(inputText);
        setGeneratedText(generated_text);
        setLoading(false);
      },
      () => {
        setText(inputText);
        setGeneratedText("");
        setLoading(false);
        console.log("No elements!");
      },
      () => {
        setText(inputText);
        setLoading(false);
        console.log("API Fail");
      }
    );
  };

  return (
    <div className={classes.root}>
      <Typography variant="h4">Text generation</Typography>
      <Typography variant='body1' className={classes.section}>
        This example use external API to perform inference on HuggingFace's servers. HuggingFace is most popular framework for SOTA NLP models. They also provide large public repository of pretrained models. 
        <p>
        The following demo uses a large, pre-trained language model to generate
        text (exactly{" "}
        <Link href="https://huggingface.co/EleutherAI/gpt-neo-2.7B">
          GPT Neo 2.7B
        </Link>{" "}
        ). While it's relatively large model (inference takes ~30s on Radeon 3900x and needs >> 20Gb of RAM) it's still quite small model compared to Microsoft's GPT3 that has over 175B parameters. For more impresive result's, you can test open-sourced 6 bilion parameter model <Link href="https://6b.eleuther.ai/">GPT-J-6B</Link>.
        </p>

        <p>
          All of the mentioned models were trained on (or at least on) <Link href="https://pile.eleuther.ai/">The Pile</Link> - a dataset that consists of both english text and code from github - so these models should be able to complete both sentences in english and code (in any major language).
        </p>
      </Typography>
      <br />
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignContent="center"
        spacing={1}
      >
        <Grid
          item
          container
          xs={1}
          justifyContent="flex-end"
          alignContent="center"
        >
          <Typography>API Key</Typography>
        </Grid>
        <Grid
          item
          container
          xs={1}
          justifyContent="flex-start"
          alignContent="center"
        >
          <Tooltip
            title="Huggingface API Key api_XXX (you must be logged in - click on the icon or Settings/API Tokens) - up to 30k input characters /month"
            onClick={(event) =>
              window.open("https://huggingface.co/settings/token")
            }
          >
            <IconButton aria-label="huggingface-forwards">
              <HelpOutlineIcon />
            </IconButton>
          </Tooltip>
        </Grid>
        <Grid item xs={10}>
          <TextField
            variant="outlined"
            fullWidth={true}
            type="password"
            value={apiKey}
            onChange={(e) => setApiKey(e.target.value)}
            placeholder="Enter API Key for Huggingface"
          ></TextField>
        </Grid>
      </Grid>
      <br />
      <TextField
        variant="outlined"
        multiline={true}
        placeholder="The meaning of life is"
        minRows="10"
        fullWidth={true}
        onChange={(e) => setInputText(e.target.value)}
        value={inputText}
      ></TextField>
      <Grid
        container
        direction="row"
        className={classes.grid}
        justifyContent="space-around"
        alignItems="center"
      >
        <MenuButton
          className={classes.sendButton}
          setInputText={setInputText}
          examples={{
            'The Lord of the Rings': 'Legolas and Gimli advanced on the orcs, raising their weapons with a harrowing war cry.',
            'Packing for trip to Mars': 'Before boarding your rocket to Mars, remember to pack these items:',
            'The worst possible outcome for Earth': 'Today, scientists confirmed the worst possible outcome: the massive asteroid will collide with Earth'
          }}
        />
        <Button
          type="submit"
          variant="contained"
          size="large"
          color="primary"
          className={classes.sendButton}
          onClick={() => show_elements()}
        >
          Submit
        </Button>
      </Grid>
      {loading && <CircularProgress />}
      {!loading && (
        <Paper elevation={5} className={classes.paper}>
          <Typography component="div" className={classes.resultBox}>
            <strong>
              {text}
            </strong>
            {generatedText}
          </Typography>
        </Paper>
      )}
    </div>
  );
}
