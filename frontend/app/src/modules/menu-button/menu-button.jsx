import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile";

export default function MenuButton(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    if (Object.keys(props.examples).includes(event.target.innerText)) {
      props.setInputText(props.examples[event.target.innerText]);
    }
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        variant="contained"
        size="large"
        color="secondary"
        className={props.className}
        startIcon={<InsertDriveFileIcon />}
        aria-controls="examples-men"
        aria-haspopup="true"
        onClick={handleClick}
      >
        Examples
      </Button>
      <Menu
        id="examples-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        {Object.keys(props.examples).map((example) => (
          <MenuItem onClick={handleClose} key={example}>
            {example}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}
