import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";

import { get_closest_elements } from "../../api/semantic_search";
import OutputList from "../list/list";
import MenuButton from "../menu-button/menu-button";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "60rem",
    margin: "auto"
  },
  sendButton: {
    marginTop: 10,
    marginBottom: 50,
  },
  tab: {
    marginLeft: 10,
    marginRight: 10,
  },
  heading: {
    marginBottom: '0.5rem',
    marginTop: "1rem",
  },
  section: {
    textAlign: 'left',
  }
}));

export default function SemanticSearchTab() {
  const classes = useStyles();
  const [query, setQuery] = useState("");
  const [elements, setElements] = useState([]);

  const show_elements = () => {
    get_closest_elements(
      query,
      (elements) => {
        setElements(elements);
      },
      () => {
        setElements([]);
        console.log("No elements!");
      },
      () => {
        console.log("API Fail");
      }
    );
  };

  return (
    <div className={classes.root}>
      <Typography variant="h4">Semantic search</Typography>
      <Typography variant="body1" className={classes.section}>
        Semantic search task is to find information in isolation from specific words. Most often, this involves assigning a vector to the text that represents its meaning in abstract form (for example, the vectors for the sentences "Al has a dog" and "She has a bulldog" will be close to each other in this space, and far from the vector assigned to, say, the sentence "The Baltic Sea is beautiful".  The whole game is to find a model that will be able to assign such vectors to sentences.
      </Typography>
      <Typography variant="h6" className={classes.heading}>How to find such vectors?</Typography>
      <Typography variant="body1"  className={classes.section}>
        In practice, there are as many approaches as there are researchers working on this topic. Currently, the best results are obtained using models based on so-called transformers. The starting point are so-called pretrained models, i.e., models that model a particular language (or many languages at the same time). In practice, vectors derived directly from such models do not work very well. (By the way, what does it mean that texts are similar?). They need to be trained on a specific domain to be released into production. For example, for an online store, the learning set might be queries (e.g., "Pink iphone cases") paired with product descriptions that match that query. Such a model is then retrained to move the query vector closer to the vector of matching descriptions, and away from the non-matching descriptions.

        <p>It is worth noting that since the method is based on vectors and not on texts, you can just as well use text to search for e.g. images (e.g. on a portal like "Vinted" where descriptions are very modest, you could search for "red pullover with santa" based only on photos of clothes)</p>
      </Typography>
      
      <Typography variant="h6" className={classes.heading}>What model is used in this demo?</Typography>
      <Typography variant="body1" className={classes.section}>
        This demo uses pretrained model called {" "} 
        <Link href="https://huggingface.co/sentence-transformers/LaBSE">
          LaBSE
        </Link>{" "} - <strong>L</strong>anguage-<strong>a</strong>gnostic <strong>B</strong>ERT <strong>s</strong>entence <strong>e</strong>mbedding. 
        The model was initially trained on a text translation search task.Vectors of sentence translations in different languages are aligned (for example, vectors generated for the expressions "Guten Morgen" and "Good morning" will be very close to each other). Vectors of sentence translations in different languages are aligned (for example, vectors for the expressions "Guten Morgen" and "Good morning" will be very close to each other). Due to the fact that translations are often not exact transpositions of words, this model works relatively well when we want to search for similar expressions, thus it can be used as a base for semantic search even without training. Additionally, it allows you to search for answers in multiple languages (specifically 109)
      </Typography>
      <Typography variant="h4" className={classes.heading}>Try for yourself!</Typography>
      <Typography variant="body1" className={classes.section}>
        This sample database consists of selected articles from Wikipedia (both in Polish and English): Frédéric Chopin, Harry Potter, Jan Krzysztof Duda, Magnus Carlsen, McDonalds, Pierogi, Politechnika Wrocławska, Queen (music), Tea, JavaScript
      </Typography>
      <br />
      <TextField
        variant="outlined"
        multiline={true}
        placeholder="Write amazing query in any language!"
        minRows="10"
        fullWidth={true}
        onChange={(e) => setQuery(e.target.value)}
        value={query}
      />
      <Grid
        container
        direction="row"
        className={classes.grid}
        justifyContent="space-around"
        alignItems="center"
      >
        <MenuButton
          setInputText={setQuery}
          examples={{
            "The best chess player in the world":
              "The best chess player in the world",
            "Język programowania": "Język programowania",
            "Czarodzieje": "Czarodzieje",
            "Hot drink": "Hot drink",
          }}
        />
        <Button
          type="submit"
          variant="contained"
          size="large"
          color="primary"
          className={classes.sendButton}
          onClick={() => show_elements()}
        >
          Submit
        </Button>
      </Grid>
      <OutputList elements={elements} />
    </div>
  );
}
