import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ImageIcon from "@material-ui/icons/Image";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";

import { inference } from "../../onnx/inference";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "60rem",
    margin: "auto"
  },
  input: {
    display: "none",
  },
  image: {
    maxHeight: 400,
    maxWidth: 500,
  },
  paper: {
    width: 200,
    padding: 20,
  },
  grid: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  section: {
    textAlign: 'left',
  }
}));

export default function ImageClassificationTab() {
  const classes = useStyles();
  const [session, setSession] = useState(null);
  const [image, setImage] = useState(null);
  const [loading, setLoading] = useState(false);
  const [output, setOutput] = useState("");

  const handleChange = (e) => {
    if (e.target.files.length) {
      setImage(URL.createObjectURL(e.target.files[0]));
    }
  };

  const classify_image = async () => {
    if (image === null) {
      setOutput("Upload image!");
      return;
    }
    setLoading(true);
    const ort = require("onnxruntime-web");
    var result;
    if (session === null) {
      const s = await ort.InferenceSession.create("./resnet18.onnx");
      setSession(s);
      result = await inference(s, image);
    } else {
      result = await inference(session, image);
    }
    setLoading(false);
    setOutput(result);
  };

  return (
    <div className={classes.root}>
      <Typography variant="h4" gutterBottom={true}>Classification on frontend</Typography>
      <Typography variant='body1' className={classes.section} gutterBottom={true}>
        This subpage use ONNX ecosystem (ONNX model format and ONNX Runtime Web
        library) to serve model on frontend side. The model for image
        classification (the smallest{" "}
        <Link href="https://arxiv.org/pdf/1512.03385.pdf">Resnet</Link> model -{" "}
        <Link href="https://pytorch.org/vision/stable/models.html#torchvision.models.resnet18">
          Resnet18
        </Link>
        ) was converted to ONNX from PyTorch framework. Models with Resnet
        architecture were trained on ImageNet dataset, which contains 1.28
        million training images with 1000 different labels (e.g., goldfish,
        komodo warbler, and Indian cobra - You can read all of the labels in{" "}
        <Link href="https://gist.github.com/yrevar/942d3a0ac09ec9e5eb3a">
          here
        </Link>
        ).
      </Typography>
      <Typography variant='body1' className={classes.section}>
        To classify image, choose image by clicking on image placeholder/preview or UPLOAD IMAGE button. Then submit image to get the model output (1 class from 1000). First classification might take a while, as model has to be downloaded to local storage. Subseqeunt calls should be quite instant on modern cpu.
      </Typography>
      <br />
      {image ? (
        <div>
          <label htmlFor="upload-image">
            <img src={image} alt="uploaded" className={classes.image} />
          </label>
        </div>
      ) : (
        <div>
          <label htmlFor="upload-image">
            <img
              src="media/placeholder.png"
              alt="dummy"
              width="500"
              height="400"
            />
          </label>
        </div>
      )}
      <input
        accept="image/*"
        type="file"
        className={classes.input}
        id="upload-image"
        onChange={handleChange}
      />
      <input
        accept="image/*"
        type="file"
        className={classes.input}
        id="contained-button-file"
        onChange={handleChange}
      />
      <Grid
        container
        direction="row"
        className={classes.grid}
        justifyContent="center"
        alignItems="center"
      >
        <label htmlFor="contained-button-file">
          <Button
            variant="contained"
            color="primary"
            size="large"
            component="span"
            endIcon={<ImageIcon />}
          >
            Upload Image
          </Button>
        </label>
        <Button
          type="submit"
          variant="contained"
          size="large"
          color="primary"
          className={classes.sendButton}
          onClick={async () => {
            await classify_image();
          }}
        >
          Submit
        </Button>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        className={classes.grid}
      >
        <Paper elevation={5} className={classes.paper}>
          {loading && <CircularProgress />}
          {!loading && <Typography component="div">{output}</Typography>}
        </Paper>
      </Grid>
    </div>
  );
}
