import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: 10,
  },
}));

export default function OutputList(props) {
  const classes = useStyles();

  const getElementsList = () => {
    let content = [];
    for (const [i, item] of props.elements.entries()) {
      let text = item.text;
      if (item.text.length > 400){
        const idx = item.text.indexOf('.', 400)  
        text = item.text.substring(0, idx + 1) + " [...]";
      }
      content.push(
        <ListItem key={i}>
          <Paper elevation={5} className={classes.paper}>
            <ListItemText primary={text} secondary={item.score} />
          </Paper>
        </ListItem>
      );
    }
    return content;
  };

  return (
    <div className={classes.root}>
      <List component="ul" aria-label="close elements to query">
        {getElementsList()}
      </List>
    </div>
  );
}
