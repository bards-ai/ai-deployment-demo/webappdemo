import './App.css';
import NavTabs from './modules/menu-bar/menu-bar'

function App() {
  return (
    <div className="App" style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
      <NavTabs />
      <footer style={{ width: "100%", background: "#777", height: "50px", paddingTop: "30px", color: "white", marginTop: "2rem" }}>
        The application is for presentation purposes only. If you have any problems, please contact us at info@bards.ai
      </footer>
    </div>
  );
}

export default App;
