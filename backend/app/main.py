from pathlib import Path

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sentence_transformers import SentenceTransformer, util
from fastapi.responses import RedirectResponse

from models import TextInput

app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


cache = {}

@app.on_event('startup')
async def _startup_event() -> None:
    print("Loading model")
    cache['embedder'] = SentenceTransformer('sentence-transformers/LaBSE')
    cache['corpus'] = []
    for filename in Path('data/').rglob('*.txt'):
        with open(filename, 'r') as input_f:
            cache['corpus'].append(input_f.read())
    cache['corpus_embeddings'] = cache['embedder'].encode(cache['corpus'], convert_to_tensor=True)
    print('Model loaded!')

@app.get('/')
def read_root():
    return RedirectResponse("/docs")


@app.post('/semantic_search')
def semantich_search_endpoint(r: TextInput):
    query_embedding = cache['embedder'].encode(r.text, convert_to_tensor=True)
    
    top_results = util.semantic_search(query_embedding, cache['corpus_embeddings'], top_k=5)[0]
    out = []
    for result in top_results:
        idx, score = result['corpus_id'], result['score']
        out.append({'text': cache['corpus'][idx], 'score': f'{score:.4f}'})
    return out