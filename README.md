# WebAppDemo

This is a web application demo project shows different ways of serving an AI model to a user. It includes an example of: serving the model on the backend, querying an external API with the model and sending the model to the frontend.

## Installation

The project uses docker and docker-compose, so you just need to build and run the containers without any additional dependencies.

0. Go to the root folder
1. Build the frontend and backend containers with the command - `docker-compose build`
2. Start the built containers with the command -  `docker-compose up`
3. Visit http://localhost:3001/

## Usage

To see a working application visit - http://localhost:3001

By default, the frontend ports are forwarded to port 3010 - http://localhost:3001 and the backend port to 8010 - http://localhost:8010.
To change the port forwarding, simply edit the docker-compose.yaml lines after the "ports" keyword.

### Semantic Search

This subpage use backend API, which serves model outputs. On the backend side, the query is encoded by a model (exactly [LaBSE](https://huggingface.co/sentence-transformers/LaBSE) - Language-agnostic BERT sentence embedding, that supports up to 109 languages). The encoded query vector is then compared to vectors of Wikipedia article texts encoded in the same way. The 5 articles that have the closest cosine distance to the query vector are shown on the page along with their score.

Selected articles from Wikipedia (both in Polish and English):

- Frédéric Chopin
- Harry Potter
- Jan Krzysztof Duda
- Magnus Carlsena
- McDonalds
- Pierogi
- Politechnika Wrocławska
- Queen (music)
- Tea
- JavaScript
### Text Generations

This subpage use external HuggingFace API. It uses a model to generate text (exactly [GPT Neo 2.7B](https://huggingface.co/EleutherAI/gpt-neo-2.7B)).
### API key for HuggingFace (IMPORTANT! - essential to run)

To use tab with "Text generation" you have to use your private API Token/Key from HuggingFace. You can use up to 30,000 characters for free in query every month. You can get that API Key from HuggingFace website. The easiest way is to click on the question mark icon on "Text generation" tab, which after logging in redirects to the subpage in the settings where the API Token is located. Simply copy it into the text field that is located at the very top of the subpage.
### Image Classification

This subpage use ONNX ecosystem (ONNX model format and [ONNX Runtime Web](https://github.com/microsoft/onnxruntime/tree/master/js/web) library) to serve model on frontend side. The model for image classification (the smallest [Resnet](https://arxiv.org/pdf/1512.03385.pdf) model - [Resnet18](https://pytorch.org/vision/stable/models.html#torchvision.models.resnet18)) was converted to ONNX format using jupyter notebook (`backend/model.ipynb`). Models with Resnet architecture were trained on [ImageNet](https://www.image-net.org) dataset, which contains 1.28 million training images with 1000 different labels (e.g., goldfish, komodo warbler, and Indian cobra - You can read all of the labels in `frontend/utils/globals.js` IMAGENET_LABELS variable). 
## Development

For developing and dynamically changing code, there is a version of docker-compose-dev.yaml that assembles files from the computer into containers to show the current code. 

0. Go to the root folder
1. Build the frontend and backend containers with the command - `docker-compose -f docker-compose-dev.yaml build`
2. Start the built containers with the command -  `docker-compose -f docker-compose-dev.yaml up`
3. Visit http://localhost:3001/